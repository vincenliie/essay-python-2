import os
import numpy as np
import cv2 as cv

from CNN import CNN
from Layers.ReluLayer import ReluLayer
from Layers.ConvolutionLayer import ConvolutionLayer
from Layers.FlattenLayer import FlattenLayer
from Layers.FullyConnectedLayer import FullyConnectedLayer
from Layers.PoolingLayer import PoolingLayer
from Layers.ConvolutionLayer import ConvolutionLayer

# easy = "Dataset/Training/Easy"
# testing = "Dataset/Testing-5"
# model_path = "Model/Model_latest.vt"

img = cv.imread('Dataset/Testing/10__M_Left_thumb_finger.BMP', cv.IMREAD_GRAYSCALE)
img = cv.resize(img, (96,96))
img = np.array(img).reshape(1,96,96,1)
img = img/255

def func_naon(x, y):
  for i in range(0, len(x[0])):
    for j in range(0, len(x[0][0])):
      print(x[0][i][j][y], end=", ")
    print()

cnn = CNN()
cnn.read_network_model_json("Model/Model_200918_new_200.vt")
cnn.predict_single(img)

hslSblmPooling = cnn.Model[5].outputLayer

print(np.array(hslSblmPooling).shape)


print("Hasil 1 :") 
func_naon(hslSblmPooling,0)
print("Hasil 2 :") 
func_naon(hslSblmPooling,1)
print("Hasil 3 :") 
func_naon(hslSblmPooling,2)
print("Hasil 15 :") 
func_naon(hslSblmPooling,14)
print("Hasil 16 :") 
func_naon(hslSblmPooling,15)
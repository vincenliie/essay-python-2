import tkinter as tk

class GUI:
  def __init__(self):
    self.window = tk.Tk()
    self.objects = []
    self.i = 0

  def show(self):
    self.window.mainloop()
  
  def insert(self, obj = None, padx = 0, pady=0):
    self.objects.append(obj)
    self.objects[self.i].pack(padx = padx, pady=pady)
    self.i += 1
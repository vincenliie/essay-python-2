from flask import Flask, render_template, request, redirect
from string import Template


import os

app = Flask(__name__)

# app.config['IMAGE_UPLOAD_LOCATION'] = url_for('static')+"/uploads"

@app.route('/', methods=['POST', 'GET'])
def build_html():

  if request.method == 'POST':
    if request.files :
      image = request.files['image']

      print(image)
      # image.save(os.path.join(app.config['IMAGE_UPLOAD_LOCATION']), image.filename)
      return redirect(request.url) 
    

  return render_template("index.html")

@app.route('/about')
def render_about():
  return render_template("about.html", name="Vincenlie Triadi")

# @app.route('/result', method=['GET','POST'])
# def build_result():

#   cnn = CNN()
#   return render_template("result.html")

# @app.route('/hello/<name>')
# def build_hello(name):
#   return Template(str(render_template("henlo.html"))).subtitute(name=name)

if __name__ == "__main__":
    app.run(debug=True, use_reloader=True, port="88")

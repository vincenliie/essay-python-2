import time

from termcolor import colored

class ProgressBar:
  def generate_equalto(self, inp, steps=20):
    str = ''
    if inp<0 :
      for i in range(0, int(steps/2)):
        str += "-"
    else:
      for i in range(0, min(inp, int(steps/2))):
        str += colored("=", "green")
      
      for i in range(int(steps/2), min(inp, int(steps/2)),-1):
        str += "-"
    
    return str

  def render_percentage(self, i):
    if i < 10:
      return """%0.3f""" % i
    else:
      return """%0.2f""" % i

  def render(self, i, total, error, steps=20, now=""):
    n = i/(total/steps)
    print('['+self.generate_equalto(int(n), steps=steps)+" "+str(self.render_percentage(i/total*100))+"% "+self.generate_equalto(int(n-(steps/2)), steps=steps)+"] "+ str(i) + "/"+str(total)+" - error : "+str(round(error, 5))+" - elapsed time : "+self.render_time(int(time.time()-now)), end=" \r")

  def render_no_time_no_error(self, i, total, steps=20):
    n = i/(total/steps)
    print('['+self.generate_equalto(int(n), steps=steps)+" "+str(self.render_percentage(i/total*100))+"% "+self.generate_equalto(int(n-(steps/2)), steps=steps)+"] "+ str(i) + "/"+str(total), end=" \r")
  
  def render_end_no_time_no_error(self, total, steps=20):
    print('['+self.generate_equalto(int(steps/2), steps=steps)+" 100% "+self.generate_equalto(int(steps/2), steps=steps)+"] "+ str(total) + "/"+str(total))
  
  def render_no_time(self, i, total, steps=20):
    n = i/(total/steps)
    print('['+self.generate_equalto(int(n), steps=steps)+" "+str(round(i/total*100, 2))+"% "+self.generate_equalto(int(n-(steps/2)), steps=steps)+"] "+ str(i) + "/"+str(total), end=" \r")

  def render_end_pb(self, total, error, steps=20, now=""):
    noe = int(time.time()- now)
    per_steps = ""
    if noe/total < 1:
      per_steps = str(int(noe/total * 1000))+"ms" # in milliseconds
    else:
      per_steps = str(int(noe/total * 1000))+"s" # in seconds

    print('['+self.generate_equalto(int(steps/2),steps)+' 100.0% '+self.generate_equalto(int(steps/2),steps)+'] '+str(total)+'/'+str(total)+" - "+str(noe)+"s - "+str(per_steps)+"/steps - error : "+str(error))
  
  def render_end_pb_no_time(self, total, steps=20):
    print('['+self.generate_equalto(int(steps/2),steps)+' 100.0% '+self.generate_equalto(int(steps/2),steps)+'] '+str(total)+'/'+str(total))

  def add_z(self, tim):
    if tim<10:
      t = '0' + str(int(tim))
    else:
      t = int(tim)
    return str(t)


  def render_time(self, time):
    if time >3600:
      h = round(time/3600, 0)
      m = round(time%3600/60, 0)
      s = round(time%3600%60, 0)

      return self.add_z(h)+":"+self.add_z(m)+":"+self.add_z(s)
    elif time > 60:
      m = round(time/60, 0);
      s = round(time%60, 0);
      return self.add_z(m)+":"+self.add_z(s)
    else:
      return "00:"+self.add_z(time)
      

  def generate_progressbar(self, total, steps = 20):
    if(steps%2 == 0):
      now = time.time()
      for i in range(0, total):
        # print(''+str(i) + '/100', end=" \r")
        self.render(i,total, steps, now)
        time.sleep(0.1)
      self.render_end_pb(total, steps, (time.time() - now))
    else:
      print("Cannot Render Progress Bar. Steps must be Even Number.")

class ReduceLROnPatience:
  def __init__(self, factor = 0.0005, patience = 5, min_lr = 0.0001, epoch_num = 0):
    pass

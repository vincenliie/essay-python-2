# import os
# import numpy as np
# from CNN import CNN
# from Layers.ReluLayer import ReluLayer
# from Layers.ConvolutionLayer import ConvolutionLayer
# from Layers.FlattenLayer import FlattenLayer
# from Layers.FullyConnectedLayer import FullyConnectedLayer
# from Layers.PoolingLayer import PoolingLayer
# from Layers.ConvolutionLayer import ConvolutionLayer

# c = ConvolutionLayer(filters = 8, input_size = (96,96,1), kernel_size=(3,3), activation='relu')
# c2 = ConvolutionLayer(filters = 8, input_size = (96,96,8), kernel_size=(3,3), activation='relu')
# p = PoolingLayer((96,96,8),stride = 2, kernel_size=2)

# cnn = CNN(num_classes = 10)
# cnn.add(c)
# cnn.add(c2)
# cnn.add(p)

# for i in cnn.Model:
#   print(str(i))

# print(cnn.summary())

from CNN import CNN

import numpy as np

cnn = CNN()
cnn.read_network_model_json("Model/Model_200918_new_200.vt")
cnn.assign_input(["Dataset/Testing-5"], train=False)

cnn.create_confusion_matrix_calculate_accuracy()
import os
import random
import matplotlib.pyplot as plt
import numpy as np
import cv2 as cv


def randoms(max_class=200, qty = 10):
  lala = "Dataset-200/Testing/"
  list_dir = os.listdir(lala)
  my_list = list(range(1, len(list_dir)))  # list of integers from 1 to 99
  # adjust this boundaries to fit your needs
  random.shuffle(my_list)

  val_id = []
  val_feature = []
  val_filename = []
  i=0
  while len(val_filename) < qty and i < len(my_list):
    subject_id, _ = list_dir[my_list[i]].split('__')
    if int(subject_id) < max_class : 
      # val_id.append(i)
      val_feature.append(plt.imread(os.path.join(lala, list_dir[my_list[i]])))
      val_filename.append(list_dir[my_list[i]])
    i+=1

  print(len(val_filename))
  print(np.array(val_filename))
  print(np.array(val_feature).shape)

  for i in range(0, len(val_filename)):
    cv.imwrite(os.path.join("Dataset/Testing-Random/",val_filename[i]), val_feature[i])

randoms(qty = 200)
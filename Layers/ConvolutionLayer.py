import numpy as np
import scipy.signal as convo
import random
import tensorflow as tf

class ConvolutionLayer():

    def __init__(self, input_size=(96,96,1), filters=1, kernel_size=(3,3), activation='relu', kernel_generator=False):
      self.width, self.height, self.depth = input_size
      self.outputSize = (self.width, self.height, filters)
      self.filters = filters
      
      if kernel_generator :
        self.kernel = self.kernel_generate(kernel_size[0], self.depth, filters)
        self.bias = self.generate_bias(filters)

      self.inputLayer = None
      self.outputLayer = None
      self.activation = activation

      self.input_grad = None
      self.kernel_grad = None
      self.output_grad = None

    def __str__(self):
      name = "Convolution Layer"
      input_sizing  = "("+str(self.height)+"x"+str(self.width)+"x"+str(self.depth)+")"
      output_sizing = "("+str(self.height)+"x"+str(self.width)+"x"+str(self.filters)+")"
      
      if len(name) > 20:
        value = name[0:20] + "\t\t" + input_sizing + "\t\t" + output_sizing
      else:
        value = name + "\t\t" + input_sizing + "\t\t" + output_sizing

      return value

    def setInput(self, inp):
        self.inputLayer = inp

    def setOutput(self, outp):
        self.outputLayer = outp

    def setInputGrad(self, inp):
        self.input_grad = inp

    def setOutputGrad(self, outp):
        self.output_grad = outp

    def setLearningRate(self, lr):
        self.learning_rate = lr

    def setKernel(self, kernel):
        self.kernel = np.array(kernel)

    def getKernel(self):
        return self.kernel
    
    def setBias(self, bias):
        self.bias = np.array(bias)

    def getBias(self):
        return self.bias

    def forward(self):
      con_1 = tf.keras.layers.Conv2D(self.filters, 3, input_shape=(self.width, self.height, self.depth), activation='relu', padding = 'same', dtype='float64', weights=[self.kernel, self.bias])(self.inputLayer)

      return con_1.numpy()

    def relu_forward(self, mtrx):
      result = []
      for a in range(0, len(mtrx)):
        hasil=[]
        for i in range(0, len(mtrx[0])):
            hasil_row = []
            for j in range(0, len(mtrx[0][0])):
                if(mtrx[a][i][j]<0):
                    hasil_row.append(0)
                else:
                    hasil_row.append(mtrx[a][i][j])
            hasil.append(hasil_row)
        result.append(hasil)
      return result

    def relu_backward(self):
      self.input_grad = None

      result = []
      for a in range(0, len(self.output_grad)):
        hasil=[]
        for i in range(0, len(self.output_grad[0])):
            hasil_row = []
            for j in range(0, len(self.output_grad[0][0])):
                if(self.output_grad[a][i][j]<=0):
                    hasil_row.append(0)
                else:
                    hasil_row.append(1)
            hasil.append(hasil_row)
        result.append(hasil)
      return result

    # def forward(self):
    #     result_arr = []
    #     # if(len(self.inputLayer) != len(self.kernel)):
    #     for i in range(0, len(self.kernel)):
    #         hasil_convo_arr = []
    #         for j in range(0, len(self.inputLayer)):
    #             hasil_convo = convo.convolve2d(
    #                 self.inputLayer[j], np.flip(self.kernel[i]), mode="same")
    #             hasil_convo_arr.append(hasil_convo)
    #         if(len(hasil_convo_arr) > 1):
    #             result = self.merger(hasil_convo_arr)
    #         else:
    #             result = hasil_convo
    #         result_arr.append(result)
    #     # else:
    #     #   for i in range(0, len(self.kernel)):
    #     #     hasil_convo = convo.convolve2d(self.inputLayer[i], np.flip(self.kernel[i]), mode="same")
    #     #     result_arr.append(hasil_convo);
    #     self.outputLayer = result_arr
    #     return self.outputLayer

    def backward(self):

      self.input_grad = None
      self.kernel_grad = None

      input_g = []
      varbl = []
      for j in range(0, len(self.inputLayer)):
          for i in range(0, len(self.kernel)):
              kernel_smtr = np.rot90(self.kernel[i], -2).copy()
              variabel = convo.convolve2d(
                  self.output_grad[i], np.flip(kernel_smtr), mode="same")
              input_g.append(variabel)
          var = self.merger(input_g)
          varbl.append(var)
      self.input_grad = varbl

      self.kernel_grad = self.bw_convolution()
      hitung_kernel = self.kernel + \
          (-self.learning_rate * np.array(self.kernel_grad))
      self.kernel = hitung_kernel.tolist()

      return self.input_grad

    def kernel_generate(self, size, input, output):
      hasil = np.zeros((size, size, input, output))  
      
      for i in range(0, size):
        for j in range(0, size):
          for k in range(0, input):
            for l in range(0, output):
              hasil[i][j][k][l] = np.random.normal(0, 0.1)
      return hasil

    def generate_bias(self, output):
      hasil = np.zeros(output)  
      
      for i in range(0, output):
        hasil[i] = random.uniform(0, 0.1)
      return hasil

    def merge(self, matrix):
        hasil_all = []
        for j in range(0, len(matrix[0])):
            hasil_row = []
            for k in range(0, len(matrix[0][0])):
                hasils = 0
                for i in range(0, len(matrix)):
                    hasils += matrix[i][j][k]
                hasil_row.append(hasils)
            hasil_all.append(hasil_row)
        return hasil_all

    def bw_convolution(self):
        result = []
        result_smtr = []

        for a in range(0, len(self.output_grad)):
            for b in range(0, len(self.inputLayer)):
                kernel_smtr = np.zeros((3, 3))
                for i in range(-1, len(self.inputLayer[b])-1):
                    for j in range(-1, len(self.inputLayer[b][0])-1):

                        for k in range(i, i+len(self.kernel[0])):  # 3
                            for l in range(j, j+len(self.kernel[0][0])):  # 3
                                # print("(k, l): "+"("+str(k)+", "+str(l)+")", end=" \r")
                                if (k < 0 or l < 0) or (k >= len(self.inputLayer[0]) or l >= len(self.inputLayer[0])):
                                    input = 0
                                else:
                                    input = self.inputLayer[b][k][l]

                                kernel_smtr[k-i][l-j] += input * self.output_grad[a][i+1][j+1]
                        result_smtr.append(kernel_smtr)
            result.append(self.merger(result_smtr))
        return result

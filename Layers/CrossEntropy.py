import numpy as np


class CrossEntropy :

  def __str__(self):
    return "Cross Entropy"

  def __init__(self, num_classes):
    self.setNumClasses(num_classes)
    self.inputLayer = None
    self.outputLayer = None
    self.targetLayer = None
    
    self.input_grad = None
    self.output_grad = None

  def setNumClasses(self, num_classes):
    self.num_classes = num_classes

  def setInput(self, inp):
    self.inputLayer = inp

  def setOutput(self,  outp):
    self.outputLayer = outp
  
  def setInputGrad(self, inp):
    self.input_grad = inp
  
  def setOutputGrad(self, inp):
    self.output_grad = inp
  
  def setTarget(self, label):
    self.targetLayer = label
    self.setNumClasses(len(label))
    
  def forward(self):
    hasil = 0
    for i in range(0,len(self.inputLayer)):
      hasil += self.targetLayer[i] * np.log(self.inputLayer[i])
    
    self.outputLayer = -hasil
    return -hasil
  
  def backward(self):
    self.input_grad = None

    # hitung = (self.targetLayer[i]*(1/self.inputLayer[i]))
    # ref : https://peterroelants.github.io/posts/cross-entropy-softmax/
    result = [self.inputLayer[i] - self.targetLayer[i] for i in range(0, self.num_classes)]
    self.input_grad = result

    # self.output_bw = result
    # print(np.array(self.inputLayer))
    # print(np.array(self.targetLayer))
    # print(np.array(self.input_grad))

    return np.array(result)

from string import Template

class CSS :
  
  def __init__(self):
    self.template = Template("<link rel='stylesheet' href='${link}'>")

  def link(self, link):
    return (self.template.substitute(link=link))
